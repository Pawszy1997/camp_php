<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/style.css">
    <title>Edit Password</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
</head>
<body>
<?php
require_once 'init.php';

$ch = curl_init("http://tank.iai-system.com/api/user/edit");
curl_setopt($ch, CURLOPT_URL, "http://tank.iai-system.com/api/user/edit");
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
$key=$_POST['new_password'];
curl_setopt($ch, CURLOPT_POSTFIELDS,
    "login=" . $_SESSION['login']."&key=".$_SESSION['key']."&new_passwords=".$_POST['new_password']);
$output = curl_exec($ch);
$zm = json_decode($output, true);
if(isset($_SESSION['login']) && isset($_SESSION['key'])){
    echo ' <form action="index.php" class="login-form" method="post">
		<h1>Edit password</h1>
		
		<div class="txtb"> 
			<input type="password" name="new_password"> 
			<span data-placeholder="New Password"></span>
		</div>
		
		<div class="txtb">
			<input type="password" name="new_password">
			<span data-placeholder="New Pasword(Again)"></span>
		</div>
		<input type="submit" class="logbtn" value="Change">
		<div class="bottom-text">
	</form>';


}

?>
<script type="text/javascript">
    $(".txtb input").on("focus", function () {
        $(this).addClass("focus");
    });
    $(".txtb input").on("blur", function () {
        if ($(this).val() == "")
            $(this).removeClass("focus");
    });
</script>
</body>
