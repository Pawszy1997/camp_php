<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style/style.css">
    <title>Create</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
</head>
<body>
<?php
session_start();
require_once 'init.php';
if(!isset($_SESSION['name'])){
    header('Location:index.php');
    exit();
}
    unset($_SESSION['errpr']);
    $chat_name = $_POST['chat_name'];
    $login = $_SESSION['name'];
    $key = $_SESSION['key'];
    echo ' <form class="login-form" >
		<h2>Create conversations</h2>
		<div class="txtb"> 
		<input type="text" name="name"> 
		<span data-placeholder="New Conversation"></span>
		</div>
		<br><br>
		<input type="submit" class="logbtn" value="Create">
		<div class="bottom-text">
		
		
	</form>';


?>
<script type="text/javascript">
    $(".txtb input").on("focus", function () {
        $(this).addClass("focus");
    });
    $(".txtb input").on("blur", function () {
        if ($(this).val() == "")
            $(this).removeClass("focus");
    });
</script>
</body>